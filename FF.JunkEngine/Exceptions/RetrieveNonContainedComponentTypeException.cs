using System;

namespace FF.JunkEngine.Exceptions
{
    public class RetrieveNonContainedComponentTypeException : ArgumentException
    {
        public RetrieveNonContainedComponentTypeException()
        {
        }

        public RetrieveNonContainedComponentTypeException(string message) : base(message)
        {
        }

        public RetrieveNonContainedComponentTypeException(string message, string paramName) : base(message, paramName)
        {
        }
    }
}