using System;

namespace FF.JunkEngine.Exceptions
{
    public class AddAlreadyContainedGameObjectException : ArgumentException
    {
        public AddAlreadyContainedGameObjectException()
        {
        }

        public AddAlreadyContainedGameObjectException(string message) : base(message)
        {
        }

        public AddAlreadyContainedGameObjectException(string message, string paramName)
            : base(message, paramName)
        {
        }
    }
}