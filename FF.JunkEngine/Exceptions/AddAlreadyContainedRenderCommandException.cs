using System;

namespace FF.JunkEngine.Exceptions
{
    public class AddAlreadyContainedRenderCommandException : ArgumentException
    {
        public AddAlreadyContainedRenderCommandException()
        {
        }

        public AddAlreadyContainedRenderCommandException(string message)
            : base(message)
        {
        }

        public AddAlreadyContainedRenderCommandException(string message, string paramName)
            : base(message, paramName)
        {
        }
    }
}