using System;

namespace FF.JunkEngine.Exceptions
{
    public class AddAlreadyContainedComponentException : ArgumentException
    {
        public AddAlreadyContainedComponentException()
        {
        }

        public AddAlreadyContainedComponentException(string message)
            : base(message)
        {
        }

        public AddAlreadyContainedComponentException(string message, string paramName)
            : base(message, paramName)
        {
        }
    }
}