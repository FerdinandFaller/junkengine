using System;

namespace FF.JunkEngine.Exceptions
{
    public class RemoveNonContainedGameObjectException : ArgumentException
    {
        public RemoveNonContainedGameObjectException()
        {
        }

        public RemoveNonContainedGameObjectException(string message) : base(message)
        {
        }

        public RemoveNonContainedGameObjectException(string message, string paramName)
            : base(message, paramName)
        {
        }
    }
}