using System;

namespace FF.JunkEngine.Exceptions
{
    public class RetrieveComponentWithNonComponentTypeException : ArgumentException
    {
        public RetrieveComponentWithNonComponentTypeException()
        {
        }

        public RetrieveComponentWithNonComponentTypeException(string message)
            : base(message)
        {
        }

        public RetrieveComponentWithNonComponentTypeException(string message, string paramName)
            : base(message, paramName)
        {
        }
    }
}