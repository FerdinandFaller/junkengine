using System;

namespace FF.JunkEngine.Exceptions
{
    public class RemoveNonContainedComponentException : ArgumentException
    {
        public RemoveNonContainedComponentException()
        {
        }

        public RemoveNonContainedComponentException(string message) : base(message)
        {
        }

        public RemoveNonContainedComponentException(string message, string paramName) : base(message, paramName)
        {
        }
    }
}