namespace FF.JunkEngine
{
    public class Line
    {
        public Line()
        {
            Start = Vector3.Zero();
            End = Vector3.Zero();
            Properties = new LineProperties();
        }

        public Line(Vector3 start, Vector3 end, LineProperties properties)
        {
            Start = start;
            End = end;
            Properties = properties;
        }

        public Vector3 Start { get; set; }
        public Vector3 End { get; set; }
        public LineProperties Properties { get; set; }
    }
}