using System;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine
{
    public class UpdateEventArgs : EventArgs
    {
        public UpdateEventArgs(GameTimer timer)
        {
            Timer = timer;
        }

        public GameTimer Timer { get; private set; }
    }
}