﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class Game : IDisposable
    {
        internal readonly List<GameObject> GameObjects = new List<GameObject>();

        protected readonly Renderer Renderer;
        protected readonly GameTimer Timer;

        private bool _disposed;
        private volatile bool _isRunning;


        protected Game(Renderer renderer, GameTimer timer)
        {
            RunnerThread = new Thread(Run) {Name = "Game Runner Thread"};
            Renderer = renderer;
            Timer = timer;
        }

        #region Dispose

        public void Dispose()
        {
            DisposeMe();
            GC.SuppressFinalize(this);
        }

        ~Game()
        {
            DisposeMe();
        }

        protected virtual void DisposeMe()
        {
            if (_disposed) return;

            // Call Dispose on all GameObjects
            for (int i = GameObjects.Count - 1; i > -1; i--)
            {
                GameObjects[i].Dispose();
            }

            Thread.Sleep(10);
            if (RunnerThread.IsAlive)
            {
                _isRunning = false;
                RunnerThread.Join();
            }

            _disposed = true;
        }

        #endregion

        public bool IsRunning
        {
            get { return _isRunning; }
            protected set { _isRunning = value; }
        }

        public Thread RunnerThread { get; protected set; }

        internal event EventHandler<UpdateEventArgs> UpdateEvent;
        internal event EventHandler<DrawEventArgs> DrawEvent;

        public ICollection<GameObject> GetReadOnlyGameObjectCollection()
        {
            return GameObjects.AsReadOnly();
        }

        protected virtual void PreUpdate()
        {
            Timer.BeginFrame();
        }

        protected virtual void Update()
        {
            if (UpdateEvent != null)
            {
                UpdateEvent(this, new UpdateEventArgs(Timer));
            }
        }

        protected virtual void Draw()
        {
            if (DrawEvent != null)
            {
                Renderer.BeginDraw();
                DrawEvent(this, new DrawEventArgs(Renderer));
                Renderer.EndDraw();
            }
        }

        protected virtual void PostDraw()
        {
            Timer.EndFrame();
        }

        public virtual void RequestStart()
        {
            RunnerThread.Start();
        }

        protected virtual void Run()
        {
            IsRunning = true;

            while (IsRunning)
            {
                PreUpdate();
                Update();
                Draw();
                PostDraw();
            }
        }

        public virtual void RequestStop()
        {
            IsRunning = false;
        }
    }
}