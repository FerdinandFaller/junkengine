namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class DrawableComponent : Component
    {
        private bool _disposed;


        protected DrawableComponent(DrawableGameObject drawableGameObject)
            : base(drawableGameObject)
        {
            drawableGameObject.ComponentsDrawEvent += OnDraw;
        }

        #region Dispose

        protected override void DisposeMe()
        {
            if (_disposed) return;

            // CleanUp
            ((DrawableGameObject) GameObject).ComponentsDrawEvent -= OnDraw;

            _disposed = true;

            base.DisposeMe();
        }

        #endregion

        protected abstract void OnDraw(object sender, DrawEventArgs drawEventArgs);
    }
}