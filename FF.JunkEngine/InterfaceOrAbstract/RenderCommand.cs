using FF.JunkEngine.Exceptions;

namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class RenderCommand
    {
        protected RenderCommand(Renderer renderer)
        {
            Renderer = renderer;
            SortOrder = 0;

            if (Renderer.RenderCommands.Contains(this))
            {
                throw new AddAlreadyContainedRenderCommandException(
                    "The RenderCommandlist of the Renderer already contains the RenderCommand");
            }
            else
            {
                Renderer.RenderCommands.Add(this);
            }
        }

        protected Renderer Renderer { get; set; }

        public int SortOrder { get; protected set; }


        internal abstract void Execute();
    }
}