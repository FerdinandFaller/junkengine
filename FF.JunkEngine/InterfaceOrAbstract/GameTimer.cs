using System;

namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class GameTimer
    {
        public DateTime GameStartTime { get; protected set; }
        public TimeSpan TimeSinceStart { get; protected set; }
        public TimeSpan DeltaTime { get; protected set; }

        public abstract void BeginFrame();
        public abstract void EndFrame();
    }
}