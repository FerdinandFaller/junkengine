﻿using System;
using FF.JunkEngine.Exceptions;

namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class Component : IDisposable
    {
        protected readonly GameObject GameObject;

        private bool _disposed;


        protected Component(GameObject gameObject)
        {
            GameObject = gameObject;

            if (GameObject.Components.Contains(this))
            {
                throw new AddAlreadyContainedComponentException(
                    "The Componentlist of the GameObject already contains the Component");
            }
            else
            {
                GameObject.Components.Add(this);
                GameObject.ComponentsUpdateEvent += OnUpdate;
            }
        }

        #region Dispose

        public void Dispose()
        {
            DisposeMe();
            GC.SuppressFinalize(this);
        }

        ~Component()
        {
            DisposeMe();
        }

        protected virtual void DisposeMe()
        {
            if (_disposed) return;

            // CleanUp
            if (GameObject.Components.Contains(this))
            {
                GameObject.Components.Remove(this);
                GameObject.ComponentsUpdateEvent -= OnUpdate;
            }
            else
            {
                // Exceptions not allowe in Dispose
                //throw new RemoveNonContainedComponentException("The Componentlist of the GameObject doesn't contain the Component");
            }

            _disposed = true;
        }

        #endregion

        protected abstract void OnUpdate(object sender, UpdateEventArgs updateEventArgs);
    }
}