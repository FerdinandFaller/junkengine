using System;
using System.Collections.Generic;
using System.Linq;
using FF.JunkEngine.Exceptions;

namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class GameObject : IDisposable
    {
        internal List<Component> Components = new List<Component>();
        public Transform Transform = new Transform();
        private bool _disposed;

        #region Init

        protected GameObject(Game game)
        {
            Game = game;

            if (Game.GameObjects.Contains(this))
            {
                throw new AddAlreadyContainedGameObjectException(
                    "The GameObjectlist of the Game already contains the GameObject");
            }
            else
            {
                Game.UpdateEvent -= OnUpdate;
                Game.UpdateEvent += OnUpdate;
                Game.GameObjects.Add(this);
            }
        }

        protected GameObject(Game game, Transform transform) : this(game)
        {
            Transform = transform;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            DisposeMe();
            GC.SuppressFinalize(this);
        }

        ~GameObject()
        {
            DisposeMe();
        }

        protected virtual void DisposeMe()
        {
            if (_disposed) return;

            // Call Dispose on all Components
            for (int i = Components.Count - 1; i > -1; i--)
            {
                Components[i].Dispose();
            }

            // CleanUp
            if (Game.GameObjects.Contains(this))
            {
                Game.UpdateEvent -= OnUpdate;
                Game.GameObjects.Remove(this);
            }
            else
            {
                // Exceptions not allowed in Dispose
                //throw new RemoveNonContainedGameObjectException("The GameObjectlist of the Game doesn't contain the GameObject");
            }

            _disposed = true;
        }

        #endregion

        public Game Game { get; protected set; }

        internal event EventHandler<UpdateEventArgs> ComponentsUpdateEvent;

        public ICollection<Component> GetReadOnlyComponentCollection()
        {
            return Components.AsReadOnly();
        }

        public Component GetComponent(Type type)
        {
            if (!type.IsSubclassOf(typeof (Component)))
            {
                throw new RetrieveComponentWithNonComponentTypeException("The given Type was not of Type 'Component'",
                                                                         type.ToString());
            }

            Component returnComponent = Components.FirstOrDefault(component => component.GetType() == type);

            if (returnComponent == null)
            {
                throw new RetrieveNonContainedComponentTypeException(
                    "The Componentlist of the GameObject doesn't contain a Type of the given Type", type.ToString());
            }

            return returnComponent;
        }


        protected virtual void OnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            if (ComponentsUpdateEvent != null)
            {
                ComponentsUpdateEvent(this, updateEventArgs);
            }
        }
    }
}