using System;

namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class DrawableGameObject : GameObject
    {
        private bool _disposed;

        #region Init

        protected DrawableGameObject(Game game) : base(game)
        {
            Game.DrawEvent -= OnDraw;
            Game.DrawEvent += OnDraw;
        }

        protected DrawableGameObject(Game game, Transform transform)
            : base(game, transform)
        {
            Game.DrawEvent -= OnDraw;
            Game.DrawEvent += OnDraw;
        }

        #endregion

        #region Dispose

        protected override void DisposeMe()
        {
            if (_disposed) return;

            // CleanUp
            Game.DrawEvent -= OnDraw;

            _disposed = true;

            base.DisposeMe();
        }

        #endregion

        public event EventHandler<DrawEventArgs> ComponentsDrawEvent;

        protected virtual void OnDraw(object sender, DrawEventArgs drawEventArgs)
        {
            if (ComponentsDrawEvent != null)
            {
                ComponentsDrawEvent(this, drawEventArgs);
            }
        }
    }
}