﻿using System.Collections.Generic;

namespace FF.JunkEngine.InterfaceOrAbstract
{
    public abstract class Renderer
    {
        internal readonly List<RenderCommand> RenderCommands = new List<RenderCommand>();

        protected Renderer(Rect rect)
        {
            Rect = rect;
        }

        protected Rect Rect { get; set; }

        /// <summary>
        /// Draw a Line
        /// </summary>
        /// <param name="line"></param>
        internal abstract void Draw(Line line);

        /// <summary>
        /// Draw a Rectangle
        /// </summary>
        /// <param name="rectangle"></param>
        /// <param name="lineProperties"></param>
        internal abstract void Draw(Rect rectangle, LineProperties lineProperties);

        /// <summary>
        /// Draw a Circle
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        internal abstract void Draw(Vector3 center, int radius);

        /// <summary>
        /// Draw a String
        /// </summary>
        /// <param name="position"></param>
        /// <param name="text"></param>
        internal abstract void Draw(Vector3 position, string text);

        /// <summary>
        /// Draw a Texture2D
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="texture"></param>
        internal abstract void Draw(Rect rect, Texture2D texture);

        public abstract void BeginDraw();
        public abstract void EndDraw();
    }
}