namespace FF.JunkEngine
{
    public class Vector3
    {
        public float X;
        public float Y;
        public float Z;


        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector3()
        {
            X = 0;
            Y = 0;
            Z = 0;
        }

        public static Vector3 Zero()
        {
            return new Vector3();
        }

        public static Vector3 One()
        {
            return new Vector3(1, 1, 1);
        }

        public static Vector3 operator *(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.X*rhs.X, lhs.Y*rhs.Y, lhs.Z*rhs.Z);
        }

        public static Vector3 operator +(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z);
        }

        public static Vector3 operator -(Vector3 lhs, Vector3 rhs)
        {
            return new Vector3(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z);
        }
    }
}