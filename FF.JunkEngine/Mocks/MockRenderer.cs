using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Mocks
{
    /// <summary>
    /// This Mock Class needs to be in the Engine Assembly, because of multiple "internal" access rights.
    /// </summary>
    public class MockRenderer : Renderer
    {
        public MockRenderer(Rect rect) : base(rect)
        {
        }

        internal override void Draw(Line line)
        {
            //throw new NotImplementedException();
        }

        internal override void Draw(Rect rectangle, LineProperties lineProperties)
        {
            //throw new NotImplementedException();
        }

        internal override void Draw(Vector3 center, int radius)
        {
            //throw new NotImplementedException();
        }

        internal override void Draw(Vector3 position, string text)
        {
            //throw new NotImplementedException();
        }

        internal override void Draw(Rect rect, Texture2D texture)
        {
            //throw new NotImplementedException();
        }

        public override void BeginDraw()
        {
            //throw new NotImplementedException();
        }

        public override void EndDraw()
        {
            //throw new NotImplementedException();
        }
    }
}