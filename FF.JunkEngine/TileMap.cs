﻿using System;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine
{
    internal class TileMap : GameObject
    {
        public Tile[,] TileArray;

        public TileMap(Game game) : base(game)
        {
            TileArray = new Tile[1,1];
            TileArray[0, 0] = new NullTile();
        }

        protected override void OnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            throw new NotImplementedException();
        }

        public static TileMap LoadFromFile()
        {
            throw new NotImplementedException();
        }

        public void SaveToFile()
        {
            throw new NotImplementedException();
        }
    }
}