namespace FF.JunkEngine
{
    public class Transform
    {
        public Transform()
        {
            Position = Vector3.Zero();
            Rotation = Vector3.Zero();
            Scale = Vector3.One();
        }

        public Transform(Vector3 position, Vector3 rotation, Vector3 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public Vector3 Scale { get; set; }
    }
}