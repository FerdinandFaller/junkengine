using System;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine
{
    public class DrawEventArgs : EventArgs
    {
        public DrawEventArgs(Renderer renderer)
        {
            Renderer = renderer;
        }

        public Renderer Renderer { get; private set; }
    }
}