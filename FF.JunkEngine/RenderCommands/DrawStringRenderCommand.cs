using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.RenderCommands
{
    public sealed class DrawStringRenderCommand : RenderCommand
    {
        private readonly Vector3 _position;
        private readonly string _text;


        public DrawStringRenderCommand(Renderer renderer, Vector3 position, string text)
            : base(renderer)
        {
            _position = position;
            _text = text;

            SortOrder = (int) (_position.Z*1000f);
        }

        internal override void Execute()
        {
            Renderer.Draw(_position, _text);
        }
    }
}