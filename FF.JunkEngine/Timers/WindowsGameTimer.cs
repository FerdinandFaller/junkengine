using System;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Timers
{
    public class WindowsGameTimer : GameTimer
    {
        private TimeSpan _beginFrameTime;


        public void Start()
        {
            GameStartTime = DateTime.Now;
            TimeSinceStart = TimeSpan.Zero;
        }

        public override void BeginFrame()
        {
            _beginFrameTime = DateTime.Now.TimeOfDay;
        }

        public override void EndFrame()
        {
            DeltaTime = DateTime.Now.TimeOfDay - _beginFrameTime;
            TimeSinceStart += DeltaTime;
        }
    }
}