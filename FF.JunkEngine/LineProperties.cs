namespace FF.JunkEngine
{
    public class LineProperties
    {
        public LineProperties()
        {
            Color = new Color();
        }

        public LineProperties(Color color)
        {
            Color = color;
        }

        public Color Color { get; set; }
    }
}