namespace FF.JunkEngine
{
    public class Rect
    {
        public float Height;
        public Vector3 Position;
        public float Width;

        public Rect()
        {
            Position = new Vector3();
            Width = 0;
            Height = 0;
        }

        public Rect(Vector3 position, float width, float height)
        {
            Position = position;
            Width = width;
            Height = height;
        }
    }
}