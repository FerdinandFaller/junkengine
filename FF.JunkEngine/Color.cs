namespace FF.JunkEngine
{
    public class Color
    {
        private float _alpha;
        private float _blue;
        private float _green;
        private float _red;

        public Color()
        {
            Red = 0;
            Green = 0;
            Blue = 0;
            Alpha = 0;
        }

        public Color(float red, float green, float blue, float alpha)
        {
            Red = red;
            Green = green;
            Blue = blue;
            Alpha = alpha;
        }

        public Color(int red, int green, int blue, int alpha)
        {
            Red = CheckAndCorrectValue(red);
            Green = CheckAndCorrectValue(green);
            Blue = CheckAndCorrectValue(blue);
            Alpha = CheckAndCorrectValue(alpha);
        }

        public float Red
        {
            get { return _red; }
            set { _red = CheckAndCorrectValue(value); }
        }

        public float Green
        {
            get { return _green; }
            set { _green = CheckAndCorrectValue(value); }
        }

        public float Blue
        {
            get { return _blue; }
            set { _blue = CheckAndCorrectValue(value); }
        }

        public float Alpha
        {
            get { return _alpha; }
            set { _alpha = CheckAndCorrectValue(value); }
        }

        private static float CheckAndCorrectValue(int value)
        {
            float returnVal;

            if (value > 255)
            {
                returnVal = 1;
            }
            else if (value < 0)
            {
                returnVal = 0;
            }
            else
            {
                returnVal = value/255.0f;
            }

            return returnVal;
        }

        private static float CheckAndCorrectValue(float value)
        {
            float returnVal;

            if (value > 1)
            {
                returnVal = 1;
            }
            else if (value < 0)
            {
                returnVal = 0;
            }
            else
            {
                returnVal = value;
            }

            return returnVal;
        }
    }
}