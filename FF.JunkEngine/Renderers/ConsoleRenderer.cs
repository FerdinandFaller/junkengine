using System;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Renderers
{
    public class ConsoleRenderer : Renderer
    {
        public ConsoleRenderer(Rect rect) : base(rect)
        {
            if (Console.LargestWindowWidth < (int) rect.Width)
            {
                rect.Width = Console.LargestWindowWidth;
            }

            if (Console.LargestWindowHeight < (int) rect.Height)
            {
                rect.Height = Console.LargestWindowHeight;
            }

            try
            {
                Console.SetBufferSize((int) rect.Width, (int) rect.Height);
                Console.SetWindowSize((int) rect.Width, (int) rect.Height);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.SetCursorPosition(0, 0);
                Console.Write(ex.Message);
            }
        }

        public override void BeginDraw()
        {
            // Clear
            RenderCommands.Clear();
            Console.Clear();
        }

        public override void EndDraw()
        {
            // Sort RenderCommands as desired
            RenderCommands.Sort(CompareRenderCommandsBySortOrder);

            // Then execute them
            foreach (RenderCommand renderCommand in RenderCommands)
            {
                renderCommand.Execute();
            }
        }

        internal override void Draw(Line line)
        {
            throw new NotImplementedException();
        }

        internal override void Draw(Rect rectangle, LineProperties lineProperties)
        {
            throw new NotImplementedException();
        }

        internal override void Draw(Vector3 center, int radius)
        {
            throw new NotImplementedException();
        }

        internal override void Draw(Vector3 position, string text)
        {
            if (position.X >= Console.BufferWidth)
            {
                position.X = Console.BufferWidth - 1;
            }

            if (position.Y >= Console.BufferHeight)
            {
                position.Y = Console.BufferHeight - 1;
            }

            try
            {
                Console.SetCursorPosition((int) position.X, (int) position.Y);
                Console.Write(text);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.SetCursorPosition(0, 0);
                Console.Write(ex.Message);
            }
        }

        internal override void Draw(Rect rect, Texture2D texture)
        {
            throw new NotImplementedException();
        }

        private static int CompareRenderCommandsBySortOrder(RenderCommand a, RenderCommand b)
        {
            int result = 0;

            if (a.SortOrder > b.SortOrder)
            {
                result = -1;
            }
            else if (a.SortOrder < b.SortOrder)
            {
                result = 1;
            }

            return result;
        }
    }
}