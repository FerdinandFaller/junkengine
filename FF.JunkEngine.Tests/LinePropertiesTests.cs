﻿using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    public class LinePropertiesTests
    {
        [Test]
        public void Color_AssignNewColor_ShouldChangeValue()
        {
            // Arrange
            var line = new Line();
            Color oldColor = line.Properties.Color;
            float newColorRed = 1.0f;

            // Act
            line.Properties.Color = new Color(newColorRed, 0.3f, 0.25f, 0.5f);

            // Assert
            Assert.AreNotSame(line.Properties.Color, oldColor);
            Assert.AreEqual(line.Properties.Color.Red, newColorRed);
        }
    }
}