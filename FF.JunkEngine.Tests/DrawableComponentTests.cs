﻿using System.Threading;
using FF.JunkEngine.InterfaceOrAbstract;
using FF.JunkEngine.Mocks;
using FF.JunkEngine.Tests.Mocks;
using FF.JunkEngine.Timers;
using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class DrawableComponentTests
    {
        private Game _game;
        private DrawableGameObject _drawGameObject;
        private DrawableComponent _drawComponent;

        public void WaitForRunnerThread()
        {
            while (!_game.IsRunning)
            {
            }
            Thread.Sleep(10);
        }

        public void EndRunnerThread()
        {
            _game.RequestStop();
            _game.RunnerThread.Join();
        }

        [Test]
        public void OnDraw_AddNewComponentToGameObjectAndRunGameOnce_OnDrawShouldOnlyBeCalledOnce()
        {
            // Arrange
            _game = new MockGameWithSleepAndRunOnce(new MockRenderer(new Rect()), new WindowsGameTimer());
            _drawGameObject = new DrawableMockGameObject(_game);
            _drawComponent = new DrawableMockComponent(_drawGameObject);

            // Act
            int componentDrawCount = ((DrawableMockComponent) _drawComponent).DrawCount;
            _game.RequestStart();
            WaitForRunnerThread();

            // Assert
            Assert.AreEqual(((DrawableMockComponent) _drawComponent).DrawCount, componentDrawCount + 1);

            EndRunnerThread();
        }
    }
}