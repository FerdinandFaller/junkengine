﻿using System;
using System.Threading;
using FF.JunkEngine.InterfaceOrAbstract;
using FF.JunkEngine.Mocks;
using FF.JunkEngine.Tests.Mocks;
using FF.JunkEngine.Timers;
using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class GameTimerTests
    {
        #region Setup/Teardown

        [SetUp]
        public void Init()
        {
            _gameTimer = new WindowsGameTimer();
            _game = new MockGameWithSleepAndRunOnce(new MockRenderer(new Rect()), _gameTimer);
        }

        #endregion

        private GameTimer _gameTimer;
        private Game _game;

        public void WaitForRunnerThread()
        {
            while (!_game.IsRunning)
            {
            }
            Thread.Sleep(10);
        }

        public void EndRunnerThread()
        {
            _game.RequestStop();
            _game.RunnerThread.Join();
        }

        [Test]
        public void EndFrame_RunGameOnce_ShouldChangeDeltaTime()
        {
            // Arrange

            // Act
            TimeSpan deltaTime = _gameTimer.DeltaTime;
            _game.RequestStart();
            WaitForRunnerThread();

            // Assert
            Assert.AreNotEqual(_gameTimer.DeltaTime, deltaTime);

            EndRunnerThread();
        }

        [Test]
        public void EndFrame_RunGameOnce_ShouldIncreaseTimeSinceStart()
        {
            // Arrange

            // Act
            TimeSpan timeSinceStart = _gameTimer.TimeSinceStart;
            _game.RequestStart();
            WaitForRunnerThread();

            // Assert
            Assert.Greater(_gameTimer.TimeSinceStart, timeSinceStart);

            EndRunnerThread();
        }
    }
}