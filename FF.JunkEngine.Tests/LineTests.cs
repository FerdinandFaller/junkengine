﻿using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class LineTests
    {
        [Test]
        public void CustomConstructor_FillWithValidParameters_EndShouldNotBeNull()
        {
            // Arrange
            var line = new Line(new Vector3(1, 1, 1), new Vector3(2, 2, 2),
                                new LineProperties(new Color(100, 100, 100, 255)));

            // Act
            // Done in Arrange

            // Assert
            Assert.NotNull(line.End);
        }

        [Test]
        public void CustomConstructor_FillWithValidParameters_PropertiesShouldNotBeNull()
        {
            // Arrange
            var line = new Line(new Vector3(1, 1, 1), new Vector3(2, 2, 2),
                                new LineProperties(new Color(100, 100, 100, 255)));

            // Act
            // Done in Arrange

            // Assert
            Assert.NotNull(line.Properties);
        }

        [Test]
        public void CustomConstructor_FillWithValidParameters_StartShouldNotBeNull()
        {
            // Arrange
            var line = new Line(new Vector3(1, 1, 1), new Vector3(2, 2, 2),
                                new LineProperties(new Color(100, 100, 100, 255)));

            // Act
            // Done in Arrange

            // Assert
            Assert.NotNull(line.Start);
        }
    }
}