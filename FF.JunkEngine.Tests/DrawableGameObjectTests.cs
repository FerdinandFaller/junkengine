﻿using System.Threading;
using FF.JunkEngine.InterfaceOrAbstract;
using FF.JunkEngine.Mocks;
using FF.JunkEngine.Tests.Mocks;
using FF.JunkEngine.Timers;
using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class DrawableGameObjectTests
    {
        private Game _game;
        private DrawableGameObject _drawGameObject;

        public void WaitForRunnerThread()
        {
            while (!_game.IsRunning)
            {
            }
            Thread.Sleep(10);
        }

        public void EndRunnerThread()
        {
            _game.RequestStop();
            _game.RunnerThread.Join();
        }

        [Test]
        public void OnDraw_AddNewGameObjectToGameAndRunGameOnce_OnDrawShouldOnlyBeCalledOnce()
        {
            // Arrange
            _game = new MockGameWithSleepAndRunOnce(new MockRenderer(new Rect()), new WindowsGameTimer());
            _drawGameObject = new DrawableMockGameObject(_game);

            // Act
            int gameObjectDrawCount = ((DrawableMockGameObject) _drawGameObject).DrawCount;
            _game.RequestStart();
            WaitForRunnerThread();

            // Assert
            Assert.AreEqual(((DrawableMockGameObject) _drawGameObject).DrawCount, gameObjectDrawCount + 1);

            EndRunnerThread();
        }
    }
}