﻿using System;
using FF.JunkEngine.InterfaceOrAbstract;
using FF.JunkEngine.Mocks;
using FF.JunkEngine.Tests.Mocks;
using FF.JunkEngine.Timers;
using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class GameTests
    {
        #region Setup/Teardown

        [SetUp]
        public void Init()
        {
            _game = new MockGameWithLessDrawCalls(new MockRenderer(new Rect()), new WindowsGameTimer());
            _gameObjectCount = _game.GetReadOnlyGameObjectCollection().Count;
        }

        #endregion

        private Game _game;
        private int _gameObjectCount;

        [Test]
        public void Dispose_AddANewGameObjectDisposeGame_GameObjectCountShouldBeZero()
        {
            // Arrange

            // Act
            _game.Dispose();

            // Assert
            Assert.AreEqual(_game.GetReadOnlyGameObjectCollection().Count, _gameObjectCount);
        }

        [Test]
        public void IsRunning_StartTheGameCheckIsRunning_ShouldBeRunning()
        {
            // Arrange
            var isRunningBeforeRunCall = _game.IsRunning;

            // Act
            _game.RequestStart();

            // Assert
            Assert.IsFalse(isRunningBeforeRunCall);
            Assert.IsTrue(_game.IsRunning);

            _game.RequestStop();
        }
    }
}