﻿using System.Threading;
using FF.JunkEngine.Exceptions;
using FF.JunkEngine.InterfaceOrAbstract;
using FF.JunkEngine.Mocks;
using FF.JunkEngine.Tests.Mocks;
using FF.JunkEngine.Timers;
using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class GameObjectTests
    {
        #region Setup/Teardown

        [SetUp]
        public void Init()
        {
            _game = new MockGameWithSleepAndRunOnce(new MockRenderer(new Rect()), new WindowsGameTimer());
            _gameObject = new MockGameObject(_game);
        }

        #endregion

        private Game _game;
        private GameObject _gameObject;
        private Component _component;
        private int _gameObjectCount;

        public void WaitForRunnerThread()
        {
            while (!_game.IsRunning)
            {
            }
            Thread.Sleep(10);
        }

        public void EndRunnerThread()
        {
            _game.RequestStop();
            _game.RunnerThread.Join();
        }

        [Test]
        public void Constructor_AddNewGameObject_ShouldIncreaseGameObjectCountByOne()
        {
            // Arrange

            // Act
            _gameObjectCount = _game.GetReadOnlyGameObjectCollection().Count;
            GameObject go = new MockGameObject(_game);

            // Assert
            Assert.AreEqual(_game.GetReadOnlyGameObjectCollection().Count, _gameObjectCount + 1);
        }

        [Test]
        public void Dispose_AddNewGameObjectThenDisposeIt_ShouldReduceGameObjectCountByOne()
        {
            // Arrange

            // Act
            _gameObjectCount = _game.GetReadOnlyGameObjectCollection().Count;
            _gameObject.Dispose();

            // Assert
            Assert.AreEqual(_game.GetReadOnlyGameObjectCollection().Count, _gameObjectCount - 1);
        }

        [Test]
        public void GetComponent_AddNewComponentGetComponentByAddedType_ReturnedComponentShouldBeTheSameAsAddedComponent
            ()
        {
            // Arrange
            _component = new MockComponent(_gameObject);

            // Act

            // Assert
            Assert.AreSame(_component, _gameObject.GetComponent(typeof (MockComponent)));
        }

        [Test]
        [ExpectedException(typeof (RetrieveNonContainedComponentTypeException))]
        public void
            GetComponent_AddNewComponentGetComponentByNonAddedType_ShouldThrowARetrieveNonContainedComponentTypeException
            ()
        {
            // Arrange
            _component = new MockComponent(_gameObject);

            // Act
            _gameObject.GetComponent(typeof (MockComponentNotAdded));

            // Assert
        }

        [Test]
        [ExpectedException(typeof (RetrieveComponentWithNonComponentTypeException))]
        public void
            GetComponent_AddNewComponentGetComponentByNonIComponentType_ShouldThrowARetrieveComponentWithNonComponentTypeException
            ()
        {
            // Arrange
            _component = new MockComponent(_gameObject);

            // Act
            _gameObject.GetComponent(typeof (WindowsGameTimer));

            // Assert
        }

        [Test]
        public void OnUpdate_AddNewGameObjectToGameAndRunGameOnce_OnUpdateShouldOnlyBeCalledOnce()
        {
            // Arrange

            // Act
            int gameObjectUpdateCount = ((MockGameObject) _gameObject).UpdateCount;
            _game.RequestStart();
            WaitForRunnerThread();

            // Assert
            Assert.AreEqual(((MockGameObject) _gameObject).UpdateCount, gameObjectUpdateCount + 1);

            EndRunnerThread();
        }
    }
}