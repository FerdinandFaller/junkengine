﻿using System.Threading;
using FF.JunkEngine.InterfaceOrAbstract;
using FF.JunkEngine.Mocks;
using FF.JunkEngine.Tests.Mocks;
using FF.JunkEngine.Timers;
using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class ComponentTests
    {
        #region Setup/Teardown

        [SetUp]
        public void Init()
        {
            _game = new MockGameWithSleepAndRunOnce(new MockRenderer(new Rect()), new WindowsGameTimer());
            _gameObject = new MockGameObject(_game);
            _component = new MockComponent(_gameObject);
        }

        #endregion

        private Game _game;
        private GameObject _gameObject;
        private Component _component;
        private int _componentCount;

        public void WaitForRunnerThread()
        {
            while (!_game.IsRunning)
            {
            }
            Thread.Sleep(10);
        }

        public void EndRunnerThread()
        {
            _game.RequestStop();
            _game.RunnerThread.Join();
        }

        [Test]
        public void Constructor_AddNewComponent_ShouldIncreaseComponentCountByOne()
        {
            // Arrange

            // Act
            _componentCount = _gameObject.GetReadOnlyComponentCollection().Count;
            Component comp = new MockComponent(_gameObject);

            // Assert
            Assert.AreEqual(_gameObject.GetReadOnlyComponentCollection().Count, _componentCount + 1);
        }

        [Test]
        public void Dispose_AddNewComponentThenDisposeIt_ShouldReduceComponentCountByOne()
        {
            // Arrange

            // Act
            _componentCount = _gameObject.GetReadOnlyComponentCollection().Count;
            _component.Dispose();

            // Assert
            Assert.AreEqual(_gameObject.GetReadOnlyComponentCollection().Count, _componentCount - 1);
        }

        [Test]
        public void OnUpdate_AddNewComponentToGameObjectAndRunGameOnce_OnUpdateShouldOnlyBeCalledOnce()
        {
            // Arrange

            // Act
            int componentUpdateCount = ((MockComponent) _component).UpdateCount;
            _game.RequestStart();
            WaitForRunnerThread();

            // Assert
            Assert.AreEqual(((MockComponent) _component).UpdateCount, componentUpdateCount + 1);

            EndRunnerThread();
        }
    }
}