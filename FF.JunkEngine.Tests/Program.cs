﻿using System.Threading;
using FF.JunkEngine.Renderers;
using FF.JunkEngine.Tests.Mocks;
using FF.JunkEngine.Timers;

namespace FF.JunkEngine.Tests
{
    internal class Program
    {
        public static void Main()
        {
            var game = new MockGameWithLessDrawCalls(new ConsoleRenderer(new Rect(new Vector3(5, 5, 0), 140, 50)),
                                                     new WindowsGameTimer());

            var go = new DrawableMockGameObject(game,
                                                new Transform(new Vector3(0, 0, 1), new Vector3(), new Vector3(1, 1, 1)));
            var component = new DrawableMockComponentMoving(go);

            var go2 = new DrawableMockGameObject(game,
                                                 new Transform(new Vector3(10, 2, 0), new Vector3(),
                                                               new Vector3(1, 1, 1)));
            var component2 = new DrawableMockComponent(go2);

            using (game)
            {
                game.RequestStart();

                // Wait for the Thread to start
                Thread.Sleep(10);
                while (game.IsRunning)
                {
                    Thread.Yield();
                }
            }
        }
    }
}