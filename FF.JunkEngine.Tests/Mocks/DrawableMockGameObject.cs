using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Tests.Mocks
{
    internal sealed class DrawableMockGameObject : DrawableGameObject
    {
        public int DrawCount;
        public int UpdateCount;


        public DrawableMockGameObject(Game game) : base(game)
        {
        }

        public DrawableMockGameObject(Game game, Transform transform)
            : base(game, transform)
        {
        }

        protected override void OnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            base.OnUpdate(sender, updateEventArgs);

            UpdateCount++;
        }

        protected override void OnDraw(object sender, DrawEventArgs drawEventArgs)
        {
            base.OnDraw(sender, drawEventArgs);

            DrawCount++;
        }

        public int GetComponentCount()
        {
            return GetReadOnlyComponentCollection().Count;
        }
    }
}