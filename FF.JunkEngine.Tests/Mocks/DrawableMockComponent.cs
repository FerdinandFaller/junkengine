using System.Globalization;
using FF.JunkEngine.InterfaceOrAbstract;
using FF.JunkEngine.RenderCommands;

namespace FF.JunkEngine.Tests.Mocks
{
    internal sealed class DrawableMockComponent : DrawableComponent
    {
        public int DrawCount;
        public int UpdateCount;

        private string _text;

        public DrawableMockComponent(DrawableGameObject drawableGameObject) : base(drawableGameObject)
        {
        }

        #region Overrides of Component

        protected override void OnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            UpdateCount++;

            _text = GameObject.Transform.Position.X.ToString(CultureInfo.InvariantCulture) + " / " +
                    GameObject.Transform.Position.Y.ToString(CultureInfo.InvariantCulture);
        }

        #endregion

        #region Overrides of DrawableComponent

        protected override void OnDraw(object sender, DrawEventArgs drawEventArgs)
        {
            DrawCount++;

            var rc = new DrawStringRenderCommand(drawEventArgs.Renderer, GameObject.Transform.Position, _text);
        }

        #endregion
    }
}