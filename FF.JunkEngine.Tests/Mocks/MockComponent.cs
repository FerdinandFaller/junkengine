using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Tests.Mocks
{
    internal sealed class MockComponent : Component
    {
        public int UpdateCount;

        public MockComponent(GameObject drawableGameObject) : base(drawableGameObject)
        {
        }

        protected override void OnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            UpdateCount++;
        }
    }
}