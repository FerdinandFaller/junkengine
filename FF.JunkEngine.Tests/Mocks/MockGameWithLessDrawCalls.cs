using System;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Tests.Mocks
{
    internal class MockGameWithLessDrawCalls : Game
    {
        public MockGameWithLessDrawCalls(Renderer renderer, GameTimer timer) : base(renderer, timer)
        {
        }

        protected override void Run()
        {
            IsRunning = true;
            TimeSpan lastDrawTime = Timer.TimeSinceStart;

            while (IsRunning)
            {
                PreUpdate();

                Update();

                // Draw only every 100 Milliseconds
                if (lastDrawTime + new TimeSpan(0, 0, 0, 0, 100) < Timer.TimeSinceStart)
                {
                    Draw();
                    lastDrawTime = Timer.TimeSinceStart;
                }

                PostDraw();
            }
        }
    }
}