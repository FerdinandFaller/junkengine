using System;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Tests.Mocks
{
    internal sealed class MockComponentNotAdded : Component
    {
        #region Implementation of IComponent

        public MockComponentNotAdded(GameObject drawableGameObject) : base(drawableGameObject)
        {
        }

        protected override void OnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}