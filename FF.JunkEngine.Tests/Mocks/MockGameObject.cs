using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Tests.Mocks
{
    internal sealed class MockGameObject : GameObject
    {
        public int UpdateCount;


        public MockGameObject(Game game)
            : base(game)
        {
        }

        public MockGameObject(Game game, Transform transform)
            : base(game, transform)
        {
        }

        protected override void OnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            base.OnUpdate(sender, updateEventArgs);

            UpdateCount++;
        }

        public int GetComponentCount()
        {
            return GetReadOnlyComponentCollection().Count;
        }
    }
}