using System.Threading;
using FF.JunkEngine.InterfaceOrAbstract;

namespace FF.JunkEngine.Tests.Mocks
{
    internal class MockGameWithSleepAndRunOnce : Game
    {
        public MockGameWithSleepAndRunOnce(Renderer renderer, GameTimer timer)
            : base(renderer, timer)
        {
        }

        protected override void Run()
        {
            IsRunning = true;

            while (IsRunning)
            {
                PreUpdate();
                Update();
                Thread.Sleep(1); // Simulate Work ;)
                Draw();
                PostDraw();

                RequestStop();
            }
        }
    }
}