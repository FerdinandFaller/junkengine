﻿using NUnit.Framework;

namespace FF.JunkEngine.Tests
{
    [TestFixture]
    internal class ColorTests
    {
        [Test]
        public void CustomFloatConstructor_FillRedWithTooHighFloatRestValid_ShouldSetRedToOneRestToGivenValues()
        {
            // Arrange
            var color = new Color(2.224f, 0.0f, 0.25f, 0.72f);

            // Act

            // Assert
            Assert.AreEqual(color.Red, 1);
            Assert.AreEqual(color.Green, 0.0f);
            Assert.AreEqual(color.Blue, 0.25f);
            Assert.AreEqual(color.Alpha, 0.72f);
        }

        [Test]
        public void CustomFloatConstructor_FillWithTooHighFloats_ShouldSetOneAsValue()
        {
            // Arrange
            var color = new Color(2.0f, 1.5f, 10.25f, 100.75f);

            // Act

            // Assert
            Assert.AreEqual(color.Red, 1);
            Assert.AreEqual(color.Green, 1);
            Assert.AreEqual(color.Blue, 1);
            Assert.AreEqual(color.Alpha, 1);
        }

        [Test]
        public void CustomFloatConstructor_FillWithTooLowFloats_ShouldSetZeroAsValue()
        {
            // Arrange
            var color = new Color(-1.0f, -0.5f, -100.25f, -100.75f);

            // Act

            // Assert
            Assert.AreEqual(color.Red, 0);
            Assert.AreEqual(color.Green, 0);
            Assert.AreEqual(color.Blue, 0);
            Assert.AreEqual(color.Alpha, 0);
        }

        [Test]
        public void CustomFloatConstructor_FillWithValidFloats_ShouldSetTheGivenParameters()
        {
            // Arrange
            var color = new Color(1.0f, 0.0f, 0.25f, 0.72f);

            // Act

            // Assert
            Assert.AreEqual(color.Red, 1.0f);
            Assert.AreEqual(color.Green, 0.0f);
            Assert.AreEqual(color.Blue, 0.25f);
            Assert.AreEqual(color.Alpha, 0.72f);
        }

        [Test]
        public void CustomIntConstructor_FillWithTooHighInts_ShouldSetOneAsValue()
        {
            // Arrange
            var color = new Color(256, 300, 1000, 475536);

            // Act

            // Assert
            Assert.AreEqual(color.Red, 1);
            Assert.AreEqual(color.Green, 1);
            Assert.AreEqual(color.Blue, 1);
            Assert.AreEqual(color.Alpha, 1);
        }

        [Test]
        public void CustomIntConstructor_FillWithTooLowInts_ShouldSetZeroAsValue()
        {
            // Arrange
            var color = new Color(-1, -255, -256, -1345);

            // Act

            // Assert
            Assert.AreEqual(color.Red, 0);
            Assert.AreEqual(color.Green, 0);
            Assert.AreEqual(color.Blue, 0);
            Assert.AreEqual(color.Alpha, 0);
        }

        [Test]
        public void CustomIntConstructor_FillWithValidInts_ShouldUseGivenParametersAndSetResultOfDivisionWith255()
        {
            // Arrange
            var color = new Color(255, 0, 123, 44);

            // Act

            // Assert
            Assert.AreEqual(color.Red, 1);
            Assert.AreEqual(color.Green, 0);
            Assert.AreEqual(color.Blue, 123/255.0f);
            Assert.AreEqual(color.Alpha, 44/255.0f);
        }

        [Test]
        public void DefaultConstructor_Use_ShouldFillRedBlueGreenAlphaWithZero()
        {
            // Arrange
            var color = new Color();

            // Act

            // Assert
            Assert.AreEqual(color.Red, 0);
            Assert.AreEqual(color.Blue, 0);
            Assert.AreEqual(color.Green, 0);
            Assert.AreEqual(color.Alpha, 0);
        }

        [Test]
        public void Red_AssignNewTooHighValue_ShouldChangeValueToOne()
        {
            // Arrange
            var color = new Color();

            // Act
            color.Red = 33;

            // Assert
            Assert.AreEqual(color.Red, 1);
        }

        [Test]
        public void Red_AssignNewTooLowValue_ShouldChangeValueToZero()
        {
            // Arrange
            var color = new Color();

            // Act
            color.Red = -33;

            // Assert
            Assert.AreEqual(color.Red, 0);
        }

        [Test]
        public void Red_AssignNewValidValue_ShouldChangeValue()
        {
            // Arrange
            var color = new Color();

            // Act
            color.Red = 0.75f;

            // Assert
            Assert.AreEqual(color.Red, 0.75f);
        }
    }
}